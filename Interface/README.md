## Interface 非空接口與空接口

eface 用於沒有方法的空接口 (empty interface)類型變量

iface 用於表示有其餘方法的接口 (interface)類型變量

```go

// $GOROOT/src/runtime/runtime2.go
type iface struct {
    tab  *itab
    data unsafe.Pointer
}

type eface struct {
    _type *_type
    data  unsafe.Pointer
}

// 補充 itab
type itab struct {
inter *interfacetype
_type *_type
hash  uint32 // copy of _type.hash. Used for type switches.
_     [4]byte
fun   [1]uintptr // variable sized. fun[0]==0 means _type does not implement inter.
}
```

以下為Go runtime示意圖

- eface

![img.png](assets/eface.png)
 
- iface

![img.png](assets/iface.png)

### 注意事項

- 由於是runtime包中的，它們的組成可能會隨著Go版本發生變化，因此這方法不一定具備版本兼容性

