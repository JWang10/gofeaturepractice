package main

import (
	"fmt"
	"runtime"
	"time"
	Trace1 "trace1"
)

func foo() {
	//defer Trace1.OldTrace("foo")()
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	bar()
}

func bar() {
	//defer Trace1.OldTrace("bar")()
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
}

func main() {
	//defer Trace1.OldTrace("instrument")()
	//defer Trace1.NewTrace()
	//defer Trace1.TraceIndent()
	//foo()
	defer func() {
		fmt.Printf("defer")
	}()
	defer TestFunc()
	println("test")
	time.Sleep(time.Second * 3)
}

func TestFunc() func() {
	return func() {
		fmt.Sprintf("g[%05d] exit: [%s]\n", 1, "instrument")
	}
}
func NewTrace() func() {
	// 借助 runtime包的幫助，實現Trace函數對它跟蹤函數名的自動獲取
	// 從deferred函數stack中取得，當參數為O，取得是Caller函數調用者的訊息：NewTrace函數，
	// 參數為1，取得NewTrace函數調用者的訊息
	pc, _, _, ok := runtime.Caller(1) // 取得goroutine stack
	if !ok {
		panic("not found caller")
	}

	fn := runtime.FuncForPC(pc)
	name := fn.Name()

	//gid := curGoroutineID()
	fmt.Printf("g[%05d] enter: [%s]\n", 1, name)
	return func() {
		fmt.Printf("g[%05d] exit: [%s]\n", 1, name)

	}
}
