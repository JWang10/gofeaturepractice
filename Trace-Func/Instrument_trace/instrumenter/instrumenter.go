package Instrumenter

type Instrumenter interface {
	Instrument(string) ([]byte, error)
}
