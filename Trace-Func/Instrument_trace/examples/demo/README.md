## Trace inject instrument

- 使用`instrument`注入`Trace`函數到要追蹤的代碼中
```shell
$ cd instrument_trace
$ go build github.com/jwang10/instrument_trace/cmd/instrument
$ instrument version 
[instrument version]
instrument [-w] xxx.go
  -w  write result to (source) file instead of stdout
```
- demo
  - `lib`放置根目錄的話，`go.mod`可以調整成以下方式
```go
module demo
go 1.18

require (
gitlab.com/jwang10/instrument_trace v0.0.0
)
replace gitlab.com/jwang10/instrument_trace => ../../


```

```go
package main

import trace "gitlab.com/jwang10/instrument_trace"

func foo() {
	defer trace.Trace()()
	bar()
}

func bar() {
	defer trace.Trace()()
}

func main() {
	defer trace.Trace()()
	foo()
}

/*
// Output
g[00001]:       ->main.main
g[00001]:               ->main.foo
g[00001]:                       ->main.bar
g[00001]:                       <-main.bar
g[00001]:               <-main.foo
g[00001]:       <-main.main

*/
```