package main

import (
	"flag"
	"fmt"
	instrumenter "gitlab.com/jwang10/instrument_trace/instrumenter"
	ast "gitlab.com/jwang10/instrument_trace/instrumenter/ast"
	"io/ioutil"
	"os"
	"path/filepath"
)

//  instrument_trace/cmd/instrument/instrument.go

var (
	wrote bool
)

func init() {
	flag.BoolVar(&wrote, "w", false, "write result to (source) file instead of stdout")
}

func usage() {
	fmt.Println("instrument [-w] xxx.go")
	flag.PrintDefaults()
}

func main() {
	fmt.Println(os.Args)
	flag.Usage = usage
	flag.Parse() // Parse the parameters from input

	if len(os.Args) < 2 { // check the count fo parameters
		usage()
		return
	}

	var file string
	if len(os.Args) == 3 {
		file = os.Args[2]
	}

	if len(os.Args) == 2 {
		file = os.Args[1]
	}
	if filepath.Ext(file) != ".go" { // check the extension is go
		usage()
		return
	}

	var ins instrumenter.Instrumenter // 聲明instrumenter.Instrumenter接口類型變量

	// 以 ast 方式實現 Instrument 接口的 ast.Instrumenter 實例
	ins = ast.New("gitlab.com/jwang10/instrument_trace", "trace", "Trace")
	newSrc, err := ins.Instrument(file) // 向Go源文件所有函数注入Trace函数
	if err != nil {
		panic(err)
	}

	if newSrc == nil {
		// add nothing to the source file. no change
		fmt.Printf("no trace added for %s\n", file)
		return
	}

	if !wrote {
		fmt.Println(string(newSrc))
		return
	}

	// 將新生成的代碼寫回原Go文件中
	if err = ioutil.WriteFile(file, newSrc, 0666); err != nil {
		fmt.Printf("write %s error: %v\n", file, err)
		return
	}
	fmt.Printf("instrument trace for %s ok\n", file)
}
