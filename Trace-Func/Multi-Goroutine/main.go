package main

import (
	"sync"
	Trace1 "trace1"
)

func A1() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	B1()
}

func B1() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	C1()
}

func C1() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	D()
}

func D() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
}

func A2() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	B2()
}
func B2() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	C2()
}
func C2() {
	//defer Trace1.NewTrace()
	defer Trace1.TraceIndent()
	D()
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		A2()
		wg.Done()
	}()

	A1()
	wg.Wait()
}
