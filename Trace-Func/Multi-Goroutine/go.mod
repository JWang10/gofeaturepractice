module Multi-Goroutine

go 1.18

require trace1 v0.0.0

replace (
	trace1 => "../Trace"
)
